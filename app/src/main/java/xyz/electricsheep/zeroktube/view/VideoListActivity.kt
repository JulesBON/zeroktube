package xyz.electricsheep.zeroktube.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.widget.ArrayAdapter
import android.widget.SearchView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.*
import xyz.electricsheep.zeroktube.BuildConfig
import xyz.electricsheep.zeroktube.R
import xyz.electricsheep.zeroktube.request.AbstractSetHostActivity
import xyz.electricsheep.zeroktube.request.RequestManager
import xyz.electricsheep.zeroktube.request.authentication.UserToken
import xyz.electricsheep.zeroktube.request.video.Description
import xyz.electricsheep.zeroktube.request.video.ZerokDatabase
import xyz.electricsheep.zeroktube.request.video.Video
import xyz.electricsheep.zeroktube.request.video.VideoIdRequest
import xyz.electricsheep.zeroktube.videolist.AbstractTokenActivity
import xyz.electricsheep.zeroktube.videolist.AbstractVideoActivity
import xyz.electricsheep.zeroktube.videolist.AbstractVideoListActivity
import xyz.electricsheep.zeroktube.videolist.VideoListAdapter
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext

/**
 * Displays a list of [Video] in [recyclerView]
 * using [viewAdapter] as an adapter and
 * [viewManager] as a layout manager and
 * starts a new [VideoMediaActivity] activity when
 * a [Video] item is clicked
 */

class VideoListActivity : AppCompatActivity(),
        AbstractVideoListActivity,
        AbstractVideoActivity,
        AbstractTokenActivity,
        AbstractSetHostActivity,
        CoroutineScope by MainScope()
{
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<VideoListAdapter.VideoListViewHolder>
    private lateinit var arrayAdapter : ArrayAdapter<Video>
    private lateinit var viewManager: RecyclerView.LayoutManager
    private val requestManager = RequestManager()
    private lateinit var zerokDatabase: ZerokDatabase
    //private lateinit var desc: Description

    override fun onCreate(savedInstanceState: Bundle?) {


        if(intent.hasExtra("host")) {
            val host = intent.getStringExtra("host")!!
            requestManager.setHost(this, host)
        }
        else {
            requestManager.getVideos(this)
        }
        super.onCreate(savedInstanceState)
        setContentView(R.layout.videolist)
    }

    /**
     * plays a [Video] from its [host] in a [VideoMediaActivity] activity
     */
    private fun videoListItemHandler(video: Video) {
        requestManager.getVideo(video.id,this)
    }

    override fun onVideoListLoaded(list: List<Video>) {
        viewManager = LinearLayoutManager(this)

        arrayAdapter = ArrayAdapter(this,android.R.layout.simple_list_item_1,list)

        viewAdapter = VideoListAdapter(
            videoSet = list,
            clickListener = { video: Video -> videoListItemHandler(video) })

        //initializes the recyclerView
        recyclerView = findViewById<RecyclerView>(R.id.recycler).apply {

            //improve performance if the content doesn't change the size of the RecyclerView layout element
            setHasFixedSize(true)

            layoutManager = viewManager
            adapter = viewAdapter

        }
    }

    override fun onVideoLoaded(video: VideoIdRequest, description: Description) {
        zerokDatabase = ZerokDatabase.getInstance(this)
        val intent = Intent(this, VideoMediaActivity()::class.java)
        launch {
            intent.putExtra("httpUrl", video.files[0].fileUrl)
            intent.putExtra("id", video.id)
            zerokDatabase.videoIdDao().deleteVideoId(video)
            if(BuildConfig.DEBUG) {
                Log.d("VideoListActivityJules", video.views.toString())
            }
            zerokDatabase.videoIdDao().insertVideo(video.id, video.uuid, video.descriptionPath, video.name, video.thumbnailPath, video.views, video.likes, video.dislikes)
            for (file in video.files) {
                zerokDatabase.filesDao().insertFile(file.fileUrl, file.fileDownloadUrl, file.resolution.label, video.id)
            }
            for (comment in video.comments) {
            }
            zerokDatabase.descriptionDao().deleteDescFromVideoId(video.id)
            zerokDatabase.descriptionDao().insertDescription(description.description, video.id)
            startActivity(intent)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        val menuItem = menu.findItem(R.id.search_icon)
        val searchView = menuItem.actionView as SearchView
        searchView.queryHint = "Search"
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String): Boolean {
                if (intent.hasExtra("host")){
                    if (query == "" ){
                        requestManager.getVideos(this@VideoListActivity, intent.getStringExtra("host"))
                    }
                    requestManager.search(query,this@VideoListActivity, intent.getStringExtra("host"))
                }
                else{
                    if (query == "" ){
                        requestManager.getVideos(this@VideoListActivity)
                    }
                    requestManager.search(query,this@VideoListActivity)
                }


                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                if (intent.hasExtra("host")){
                    if (query == "" ){
                        requestManager.getVideos(this@VideoListActivity, intent.getStringExtra("host"))
                    }
                    requestManager.search(query,this@VideoListActivity, intent.getStringExtra("host"))
                }
                else{
                    if (query == "" ){
                        requestManager.getVideos(this@VideoListActivity)
                    }
                    requestManager.search(query,this@VideoListActivity)
                }


                return false
            }
        })

        return true
    }

    override fun onTokenLoaded(token: UserToken) {
        if(BuildConfig.DEBUG) {
            Log.i("VideoListActivityMarius", "onTokenLoaded => token loaded")
        }
        if(intent.hasExtra("subscriptions")){
            if(BuildConfig.DEBUG) {
                Log.d("VideoListActivityMarius", "onTokenLoaded => getSubscriptionFeed")
            }
            requestManager.getSubscriptionFeed(this)
        }
        else {
            requestManager.getVideos(this)
        }
    }

    override fun onHostSet() {
        if(intent.hasExtra("subscriptions")) {
            if(intent.getBooleanExtra("subscriptions",false)) {
                //for testing purposes :
                val splogs = this.getSharedPreferences("logs",0)
                val username = splogs.getString("username","")!!
                val password = splogs.getString("password","")!!
                if(BuildConfig.DEBUG) {
                    Log.d("VideoListActivityMarius", "onHostSet => intent subscriptions OK")
                }

                requestManager.fetchUserToken(this, username, password)
            }
        }
        else {
            requestManager.getVideos(this)
        }
    }
}
