package xyz.electricsheep.zeroktube.request.video

import android.content.IntentFilter
import retrofit2.Call
import retrofit2.http.*

interface ListVideoService {
    @GET("api/v1/{path}videos")
    fun listVideos(@Path("path") path:String, @HeaderMap authorization: Map<String,String>, @Query("start") start: Int, @Query("count") count: Int): Call<VideoListRequest>
}