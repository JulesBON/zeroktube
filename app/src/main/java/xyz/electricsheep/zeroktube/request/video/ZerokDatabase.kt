package xyz.electricsheep.zeroktube.request.video

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [VideoFiles::class, VideoIdRequest::class, Description::class], version = 1, exportSchema = false)
abstract class ZerokDatabase : RoomDatabase(){
    abstract fun filesDao() :VideoFilesDao
    abstract fun videoIdDao() : VideoIdRequestDao
    abstract fun descriptionDao() : DescriptionDao


    companion object {
        private var instance : ZerokDatabase? = null

        fun getInstance(context: Context) : ZerokDatabase {
            if (instance == null) {
                instance = Room.databaseBuilder(
                        context.applicationContext,
                        ZerokDatabase::class.java,
                        "filesDatabase"
                ).build()
            }
            return instance!!
        }
    }
}