package xyz.electricsheep.zeroktube.request.video

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import xyz.electricsheep.zeroktube.request.comments.Comment


@Entity(tableName = "videoIdRequest")
data class VideoIdRequest (@PrimaryKey var id: Int,
                           var uuid : String,
                           var descriptionPath: String?,
                           var name: String?,
                           var thumbnailPath: String,
                           @Ignore var files: List<VideoFiles>,
                           var views: Int,
                           var likes: Int,
                           var dislikes: Int,
                           @Ignore var comments : List<Comment>) {
    constructor() : this(-1,"","","","",listOf(), -1, -1, -1, listOf())
}