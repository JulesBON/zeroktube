package xyz.electricsheep.zeroktube.choose_instance.instanceList

import xyz.electricsheep.zeroktube.choose_instance.instance.Instance

interface AbstractInstanceActivity {
    fun onInstanceLoaded(inst: Instance)
}


