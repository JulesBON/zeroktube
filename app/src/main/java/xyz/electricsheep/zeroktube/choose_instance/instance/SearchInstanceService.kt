package xyz.electricsheep.zeroktube.choose_instance.instance

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface SearchInstanceService {
    @GET("instances")
    fun searchInstance(@Query("search") query : String): Call<InstanceListRequest>
}