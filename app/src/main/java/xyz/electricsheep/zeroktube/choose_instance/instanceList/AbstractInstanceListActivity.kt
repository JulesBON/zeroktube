package xyz.electricsheep.zeroktube.choose_instance.instanceList

import xyz.electricsheep.zeroktube.choose_instance.instance.Instance


interface AbstractInstanceListActivity {
    fun onInstanceListLoaded(list: List<Instance>)
}
